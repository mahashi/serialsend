﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerialSend
{
    public partial class Checksum : Form
    {
        public Checksum()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string stringToCalc = textBox1.Text;
            int pointer = 0, len = stringToCalc.Length;
            int checksum = 0;
            bool isDollar = false;
            bool isStar = false;
            if (stringToCalc[0] == '$')
            {
                ++pointer;
                isDollar = true;
            }

            if(stringToCalc[stringToCalc.Length-1] == '*')
            {
                --len;
                isStar = true;
            }

            while (len > pointer)
            {
                checksum ^= Convert.ToByte(stringToCalc[pointer]);
                ++pointer;
            }
            textBox2.Clear();

            if(!isDollar)
                textBox2.Text += "$";

            textBox2.Text += stringToCalc;

            if(!isStar)
                textBox2.Text += "*";

            textBox2.Text += checksum.ToString("X");
        }
    }
}
