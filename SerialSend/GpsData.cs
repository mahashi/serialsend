﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialSend
{
    class GpsData
    {
        public bool IsUsed { get; set; }
        public string Name {get; set;}

        public GpsData(string name, bool isUsed)
        {
            this.Name = name;
            this.IsUsed = isUsed;
        }
    }
}
